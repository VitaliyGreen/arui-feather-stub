module.exports = {
    server: {
        port: 8080
    },
    proxyAssets: {
        host: 'localhost',
        port: 9090
    },
    devtools: true,
    client: {
        authPage: '/',
        contextRoot: ''
    }
};
